import React from 'react'
import './App.scss'

import Calendar from './components/Calendar'

const App = () => {
  return (
    <div className="container">
      <Calendar></Calendar>
    </div>
  )
}

export default App
