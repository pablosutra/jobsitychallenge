import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from './reducers'

const middleware = applyMiddleware(thunkMiddleware)

const enhancer = compose(middleware)

const store = createStore(rootReducer, enhancer)

export default store
