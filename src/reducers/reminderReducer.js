import { ADD_REMINDER, EDIT_REMINDER } from '../constants'

const initialState = {
  reminders: [],
}

export const reminderReducer = (state = initialState, action) => {
  const { type, reminder } = action
  switch (type) {
    case ADD_REMINDER:
      return { reminders: [...state.reminders, reminder] }
    case EDIT_REMINDER: {
      const updatedReminders = state.reminders.map((rem) => {
        if (rem.id === reminder.id) {
          return reminder
        }
        return rem
      })
      return { ...state, reminders: updatedReminders }
    }
    default:
      return { ...state }
  }
}
