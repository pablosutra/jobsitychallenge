import React, { useState, useEffect } from 'react'
import { boolean, object, func } from 'prop-types'
import classNames from 'classnames'
import DatePicker from 'react-datepicker'
import { CompactPicker } from 'react-color'
import { v4 as uuid } from 'uuid'
import moment from 'moment'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reminderActions } from '../../actions'
import axios from 'axios'
import { weatherAPI } from '../../constants'
const { REACT_APP_WEATHER_API_KEY } = process.env
const ReminderModal = ({ showModal, reminder, onClose, actions }) => {
  const modalClasses = classNames('modal is-clipped', {
    'is-active': showModal,
  })
  const [reminderInfo, setReminderInfo] = useState(reminder)
  const [weather, setWeather] = useState()
  const saveReminder = () => {
    if (!reminder) {
      actions.createReminder({ ...reminderInfo, id: uuid() })
    } else {
      actions.editReminder({ ...reminder, ...reminderInfo })
    }
    setReminderInfo(null)
    onClose()
  }
  const _checkWeather = (city) => {
    axios
      .get(`${weatherAPI}/?q=${city}&appid=${REACT_APP_WEATHER_API_KEY}`)
      .then((response) => setWeather(response.data.weather[0]))
      .catch(() => setWeather(null))
  }
  useEffect(() => {
    if (weather) {
      setReminderInfo({ ...reminderInfo, weather })
    }
  }, [weather])

  return (
    <div className={modalClasses}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{reminder ? 'Edit Reminder' : 'Add Reminder'}</p>
          <button className="delete" aria-label="close" onClick={onClose}></button>
        </header>
        <section className="modal-card-body">
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Date:</label>
            </div>
            <div className="field-body">
              <div className="field">
                <DatePicker
                  minDate={moment().startOf('month').toDate()}
                  maxDate={moment().endOf('month').toDate()}
                  onChange={(date) => {
                    setReminderInfo({ ...reminderInfo, date })
                  }}
                  selected={reminderInfo?.date}
                  className="input"
                  dateFormat="MMM dd, yyyy"
                  placeholderText="Reminder date"
                />
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Date:</label>
            </div>
            <div className="field-body">
              <div className="field">
                <DatePicker
                  showTimeSelect
                  showTimeSelectOnly
                  placeholderText="Reminder time"
                  dateFormat="HH:mm"
                  timeIntervals={30}
                  onChange={(time) => {
                    setReminderInfo({ ...reminderInfo, time })
                  }}
                  selected={reminderInfo?.time}
                  className="input"
                />
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Color:</label>
            </div>
            <div className="field-body">
              <div className="field">
                <CompactPicker
                  color={reminderInfo?.color}
                  onChange={(color) => setReminderInfo({ ...reminderInfo, color: color.hex })}
                />
              </div>
            </div>
          </div>

          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">City:</label>
            </div>
            <div className="field-body">
              <div className="field">
                <input
                  className="input half-width"
                  onChange={(ev) =>
                    setReminderInfo({ ...reminderInfo, city: ev.currentTarget.value })
                  }
                  placeholder="City"
                  onBlur={(ev) => _checkWeather(ev.currentTarget.value)}
                  defaultValue={reminderInfo?.city}
                />
                {weather && (
                  <img
                    src={`http://openweathermap.org/img/wn/${weather.icon}.png`}
                    alt={weather.description}
                    title={weather.description}
                  />
                )}
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label">Reminder:</label>
            </div>
            <div className="field-body">
              <div className="field">
                <input
                  className="input"
                  onChange={(ev) =>
                    setReminderInfo({ ...reminderInfo, reminder: ev.currentTarget.value })
                  }
                  placeholder="Reminder Text"
                  maxLength={30}
                  defaultValue={reminderInfo?.text}
                />
              </div>
            </div>
          </div>
        </section>
        <footer className="modal-card-foot">
          <button className="button is-success" onClick={saveReminder}>
            Save
          </button>
          <button className="button" onClick={onClose}>
            Cancel
          </button>
        </footer>
      </div>
    </div>
  )
}

ReminderModal.propTypes = {
  showModal: boolean,
  reminder: object,
  onClose: func,
  actions: object,
}

ReminderModal.defaultProps = {
  showModal: false,
  reminder: null,
}

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(reminderActions, dispatch),
})

export default connect(null, mapDispatchToProps)(ReminderModal)
