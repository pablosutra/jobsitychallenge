import React, { Fragment, useState } from 'react'
import { array } from 'prop-types'
import moment from 'moment'
import { connect } from 'react-redux'
import { DISPLAY_CALENDAR_DAYS, daysOfWeek } from '../../constants'
import ReminderModal from '../ReminderModal'
import _filter from 'lodash/filter'

const currentDay = moment()
const startDay = moment().startOf('month').day()
const monthDays = currentDay.daysInMonth()
const daysInRow = new Array(DISPLAY_CALENDAR_DAYS).fill()
const needsExtraRow = startDay > DISPLAY_CALENDAR_DAYS / 2
const rows = Math.ceil(monthDays / DISPLAY_CALENDAR_DAYS)
const totalRows = needsExtraRow ? rows + 1 : rows
const calendarRows = new Array(totalRows).fill()

const Calendar = ({ reminders }) => {
  const [showReminderModal, setShowReminderModal] = useState(false)
  const [reminder, setReminder] = useState(null)

  const _addReminder = () => {
    setShowReminderModal(true)
  }

  const _editReminder = (reminder) => {
    setReminder(reminder)
    setShowReminderModal(true)
  }

  return (
    <Fragment>
      <ReminderModal
        showModal={showReminderModal}
        reminder={reminder}
        onClose={() => setShowReminderModal(false)}
      />
      <button onClick={_addReminder} className="button is-primary mb-4 mt-4">
        Add Reminder
      </button>
      <table className="table is-bordered is-fullwidth">
        <thead>
          <tr className="has-background-link-light is-size-4 has-text-centered">
            <td className="has-text-weight-bold" colSpan={DISPLAY_CALENDAR_DAYS}>
              {currentDay.format('MMMM')}
            </td>
          </tr>
          <tr>
            {daysInRow.map((_, idx) => (
              <td
                className="has-text-centered has-background-link has-text-white"
                key={Math.random()}
              >
                {daysOfWeek[idx]}
              </td>
            ))}
          </tr>
        </thead>
        <tbody>
          {calendarRows.map((_, row) => (
            <tr key={Math.random()}>
              {daysInRow.map((_, day) => {
                const currDay = row * DISPLAY_CALENDAR_DAYS + day + 1 - startDay
                const dateReminders = _filter(reminders, (rem) =>
                  moment(rem.date).isSame(moment().date(currDay).startOf('day')),
                )
                return (
                  <td key={Math.random()}>
                    {currDay <= monthDays && currDay > 0 && (
                      <div>
                        {currDay}
                        {dateReminders.map((rem) => (
                          <button
                            className="button is-block is-fullwidth mb-4 "
                            key={rem.id}
                            style={{ backgroundColor: rem.color }}
                            onClick={() => _editReminder(rem)}
                          >
                            <div>
                              {moment(rem.time).format('HH:mm')}
                              {rem.weather && (
                                <figure className="image is-24x24 is-inline-block">
                                  <img
                                    src={`http://openweathermap.org/img/wn/${rem.weather.icon}.png`}
                                    alt={rem.weather.description}
                                    title={rem.weather.description}
                                  />
                                </figure>
                              )}
                            </div>
                          </button>
                        ))}
                      </div>
                    )}{' '}
                  </td>
                )
              })}
            </tr>
          ))}
        </tbody>
      </table>
    </Fragment>
  )
}
Calendar.propTypes = {
  reminders: array,
}

Calendar.defaultProps = {
  reminders: [],
}

const mapStateToProps = ({ reminders }) => ({
  reminders: reminders.reminders,
})
export default connect(mapStateToProps)(Calendar)
