export const DISPLAY_CALENDAR_DAYS = 7
export const daysOfWeek = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
]
export const weatherAPI = 'https://openweathermap.org/data/2.5/weather'
export * from './types'
