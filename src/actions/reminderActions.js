import { ADD_REMINDER, EDIT_REMINDER } from '../constants/index'

export const createReminder = (reminder) => ({ type: ADD_REMINDER, reminder })

export const editReminder = (reminder) => ({ type: EDIT_REMINDER, reminder })
